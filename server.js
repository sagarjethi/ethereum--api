import express from "express";
import bodyParser from "body-parser";
import moment from "moment";
import * as config  from "./config"
import mmtABI  from "./MMTABI";

let cors = require("cors");

global.__base = __dirname;

const app = express();
const server = require("http").createServer(app);

import EthereumClient from "./etherClient";

const logRequest = (req, res, next) => {
	const app = req.app;
	let ip = "Unknown";
	try {
		ip = req.headers && req.headers["x-forwarded-for"] && req.headers["x-forwarded-for"].split(",").pop() || req.connection && req.connection.remoteAddress || req.socket && req.socket.remoteAddress || req.connection && req.connection.socket && req.connection.socket.remoteAddress;
	} catch (exe) {
		// fail silently
	}

	req.headers["ip-address"] = ip || "Unknown";
	if (!req.originalUrl.includes("/kue-api") && req.method != "OPTIONS") {
		console.log("REQUEST", `${moment().format("DD/MM/YY HH:mm:ss")} => ${req.method} from ${req.originalUrl} where ip is ${ip}`);
		res.on("finish", () => {
			if (app.queueClient) app.queueClient.push({ ...req.headers, requestUrl: req.originalUrl, statusCode: res.statusCode, contentLength: res.get("Content-Length") || 0 });
			console.log("REQUEST_FINISH", `${moment().format("DD/MM/YY HH:mm:ss")} => ${res.statusCode} for ${req.originalUrl} having ${res.statusMessage}; ${res.get("Content-Length") || 0}b sent`);
		});
	}

	// pass middleware
	next();
};

// Middleware
app.use(cors());
app.use(logRequest);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// handle routes
app.get("/checkEtherBalance/:ethereumAddress", async (req, res, next) => {
	if (req.params.ethereumAddress) {
		const transactions = await EthereumClient.checkEtherBalance(req.params.ethereumAddress)
		res.status(200).json({ result: transactions,status:true });
	} else {
		res.status(400).json({ result: false,status:false });
	}
});

app.get("/getTransactionsByAccount/:ethereumAddress", async (req, res, next) => {
	if (req.params.ethereumAddress) {
		try {
			const transactions = await EthereumClient.getTransactionsByAccount(req.params.ethereumAddress)
			res.status(200).json({ result: transactions,status:true });

		}
		catch (error) {
			console.log(error);
			res.status(400).json({ error: error,status:false});
		}
	} else {
		res.status(400).json({ result: false, status:false });
	}
});

app.get("/getTransaction/:transactionHash", async (req, res, next) => {
	if (req.params.transactionHash) {
		try {
			const transactions = await EthereumClient.getTransaction(req.params.transactionHash)
			res.status(200).json({ result: transactions,status:true });

		}
		catch (error) {
			console.log(error);
			res.status(400).json({ error: error,status:false});
		}
	} else {
		res.status(400).json({ result: false, status:false });
	}
});

app.get("/getTransactionStatus/:transactionHash", async (req, res, next) => {
	if (req.params.transactionHash) {
		try {
			const transactions = await EthereumClient.getTransactionStatus(req.params.transactionHash)
			res.status(200).json({ result: transactions,status:true  });

		}
		catch (error) {
			console.log(error)
			res.status(500).json({ error: error,status:false });
		}
	} else {

		res.status(400).json({ result: false,status:false });
	}
});
app.post("/createWallet", async (req, res, next) => {
	try {
		const transaction = await EthereumClient.createWallet();

		let wallet={
			address:transaction.address,
			privateKey:transaction.privateKey
		};
		res.status(200).json({ result:wallet,status:true});
	}
	catch (error) {
		res.status(500).json({ error: error,status:false  });
	}

});
app.post("/etherTransfer", async (req, res, next) => {
	if (req.body.recipientAddress && req.body.address && req.body.privateKey && req.body.value) {
		try {
			const transaction = await EthereumClient.etherTransfer(req.body.recipientAddress, req.body.value, req.body.address, req.body.privateKey);
			res.status(200).json(transaction);
		}
		catch (error) {
			console.log(error);
			res.status(500).json({error: error,status:false });
		}
	} else {
		res.status(400).json({ result: false,status:false });
	}
});

app.post("/transferBNBToken", async (req, res, next) => {
	if (req.body.address && req.body.privateKey && req.body.walletAddress,req.body.amount) {
		try {
			let contractAddress=config.bnbAbiContractAddress;
			let contractABI=bnbABI;
			let baseFactor=18;
			const transaction = await EthereumClient.transferToken(contractAddress, contractABI, req.body.address, req.body.amount, baseFactor, req.body.walletAddress,req.body.privateKey);
			res.status(200).json({ result: transaction,transactionLink:"https://etherscan.io/tx/"+transaction,status:true });

		} catch (error) {

			res.status(200).json({ error: error, status:false});
		}

	}
	else {
		res.status(400).json({ result: false,status:false });
	}
});

app.post("/transferMMTToken", async (req, res, next) => {
	if (req.body.address && req.body.privateKey && req.body.walletAddress,req.body.amount) {
		try {
			let contractAddress=config.mmtAbiContractAddress;
			let contractABI=mmtABI;
			let baseFactor=18;
			const transaction = await EthereumClient.transferToken(contractAddress, contractABI, req.body.address, req.body.amount, baseFactor, req.body.walletAddress,req.body.privateKey);
			res.status(200).json({ result: transaction,transactionLink:"https://etherscan.io/tx/"+transaction,status:true });

		} catch (error) {

			res.status(200).json({ error: error, status:false});
		}

	}
	else {
		res.status(400).json({ result: false,status:false });
	}
});

app.get("/getBalanceOfMMTToken/:ethereumAddress", async (req, res, next) => {
	if (req.params.ethereumAddress) {
		try {
			let contractAddress=config.mmtAbiContractAddress;
			let contractABI=mmtABI;
			let baseFactor=18;
			const transaction = await EthereumClient.getTokenBalanceToken(contractAddress, contractABI,req.params.ethereumAddress,baseFactor);
			res.status(200).json({ result: transaction,status:true });

		} catch (error) {

			res.status(200).json({ error: error, status:false});
		}

	}
	else {
		res.status(400).json({ result: false,status:false });
	}
});

server.listen("9000", "0.0.0.0", async () => {
	console.log("APP", `Server running on port "9000" and on host "0.0.0.0"`);
	process.on("unhandledRejection", (reason, promise) => {
		console.log("APP_ERROR", `Uncaught error in ${String(reason)}`, promise);
	});
});
