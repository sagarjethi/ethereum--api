module.exports = {
port: process.env.PORT || 9000,
host: process.env.HOST || "127.0.0.1",
mongoConfig: {
uri: ""
},
mongoOptions: {
},
web3Config: {
localUrl: "http://localhost:8545",
infuraRopstenUrl: "https://ropsten.infura.io/Jy5YYgH6Xcj9sCLegGbH",
infuraRinkebyUrl: "https://rinkeby.infura.io/Jy5YYgH6Xcj9sCLegGbH",
infuraKovanbyUrl: "https://kovan.infura.io/Jy5YYgH6Xcj9sCLegGbH",
infureMainUrl: "https://mainnet.infura.io/idreBd9RUeiYarmnqYW1",
},
kueConfig: {
redis: {
host: "127.0.0.1",
port: "6379",
},
},
accounts: {
transaferAccountPublicKey: '',
transferAccountPrivateKey: '',
coldAccountPublicKey: '',
},
chainList :{
        mainnet: 1,
        morden: 2,
        ropsten: 3,
        rinkeby: 4,
        ubiqMainnet: 8,
        ubiqTestnet: 9,
        rootstockMainnet: 30,
        rootstockTestnet: 31,
        kovan: 42,
        ethereumClassicMainnet: 61,
        ethereumClassicTestnet: 62,
        ewasmTestnet: 66,
        gethPrivateChains: 1337
},
utils: {
walletSalt: "6296661279",
bugSnagApiKey: "",
etherScanApiKey: "",
etherScanUrl: "http://api.etherscan.io/api?module=account&action=txlist&sort=asc&apikey=M81XFGHSD359SKMC917D5YM467NRM6QAE6",
withdrawBackoff: 20000,
depositBackoff: 20000,
balanceFactor: 1.18,
failFactor: 0.02,
gasFactor: 1.12,
chainId: 12,
depositConfirmation: 40,
depositAttempt: 3,
withdrawAttempt: 3,
sweepAttempt: 5,
notifyAttempt: 10,
transferAccountBalanceLimit: 50,
depositDiscoverInterval: '30 */2 * * * *',
depositConfirmationInterval: '* * * * *',
withdrawDiscoverInterval: '30 */3 * * * *',
apiPrefix: '/api/ether',
depositFreezed: 'deposit_freezed',
withdrawFreezed: 'withdraw_freezed'
},
bnbAbiContractAddress:"0xB8c77482e45F1F44dE1745F52C74426C631bDD52",
mmtAbiContractAddress:"0x683e77A8C1F17c6250074B60A700f62be0500878",

};
