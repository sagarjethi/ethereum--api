import Web3 from "web3";
import BigNumber from "bignumber.js";
import * as config  from "./config";



class EthereumClient {

    constructor() {
	       this.web3();
      //  console.log(config);
	}

	async web3() {
        this.web3 = new Web3(new Web3.providers.HttpProvider(config.web3Config.infureMainUrl));
        let status = await this.web3.eth.net.isListening();
        if (status) {
            console.log("Success");
        } else {
            console.error("Failed");
        }
    }

    createWallet() {
        return new Promise((resolve, reject) => {
            resolve(this.web3.eth.accounts.create());
        });
    }

    checkValidEthereumAddress(address) {
        return new Promise((resolve, reject) => {
            try {
                resolve(this.web3.utils.isAddress(address));
            } catch (e) {
                resolve(false);
            }
        });
    }

    encryptWallet(privateKey,password) {
        return new Promise((resolve, reject) => {
            resolve(this.web3.eth.accounts.encrypt(privateKey, password));
        });
    }

    decryptWallet(encryptedPrivateKey,password) {
        return new Promise((resolve, reject) => {
            resolve(this.web3.eth.accounts.decrypt(encryptedPrivateKey, password));
        });
    }

    checkEtherBalance(address) {
        console.log("-----in----");
        return new Promise(async (resolve, reject) => {
            try {
                console.log("-----in promise----");
                let balance = await this.web3.eth.getBalance(address);
                resolve(this.web3.utils.fromWei(balance, 'ether'));
            } catch (e) {
                resolve(e);
            }
        })
    }

    getTransactionStatus(txHash) {
        return new Promise(async (resolve, reject) => {
            try {
                let receipt = await this.web3.eth.getTransactionReceipt(txHash);
                if (receipt) {
                    return resolve([receipt]);
                } else {
                    reject(new Error('TRANSACTION NOT FOUND'));
                }
            } catch (e) {
                reject(e);
            }
        })
    }

    getTransactionsByAccount(ethereumAddress,startBlockNumber=null, endBlockNumber=null) {
        return new Promise(async (resolve, reject) => {
            try {
                if (endBlockNumber == null) {
                    endBlockNumber = this.web3.eth.blockNumber;
                    console.log("Using endBlockNumber: " + endBlockNumber);
                }
                if (startBlockNumber == null) {
                    startBlockNumber = endBlockNumber - 15000;
                    console.log("Using startBlockNumber: " + startBlockNumber);
                }
                console.log("Searching for transactions to/from account \"" + ethereumAddress + "\" within blocks "  + startBlockNumber + " and " + endBlockNumber);
                let transactionsList=[];
                for (var i = startBlockNumber; i <= endBlockNumber; i++) {
                    if (i % 1000 == 0) {
                        console.log("Searching block " + i);
                    }
                    var block = this.web3.eth.getBlock(i, true);
                    if (block != null && block.transactions != null) {
                        block.transactions.forEach( function(e) {
                            if (ethereumAddress == "*" || ethereumAddress == e.from || ethereumAddress == e.to) {
                                transactionsList.push(e);
                                console.log("  tx hash          : " + e.hash + "\n"
                                    + "   nonce           : " + e.nonce + "\n"
                                    + "   blockHash       : " + e.blockHash + "\n"
                                    + "   blockNumber     : " + e.blockNumber + "\n"
                                    + "   transactionIndex: " + e.transactionIndex + "\n"
                                    + "   from            : " + e.from + "\n"
                                    + "   to              : " + e.to + "\n"
                                    + "   value           : " + e.value + "\n"
                                    + "   time            : " + block.timestamp + " " + new Date(block.timestamp * 1000).toGMTString() + "\n"
                                    + "   gasPrice        : " + e.gasPrice + "\n"
                                    + "   gas             : " + e.gas + "\n"
                                    + "   input           : " + e.input);
                            }
                        });
                    }
                }
                return resolve(transactionsList);

            } catch (e) {
                reject(e);
            }
        })
    }

    getTransaction(txHash) {
        return new Promise(async (resolve, reject) => {
            try {
                let transaction = await this.web3.eth.getTransaction(txHash);
                if (transaction) {
                    resolve(transaction)
                } else {
                    reject(new Error('TRANSACTION NOT FOUND'));
                }
            } catch (e) {
                reject(e)
            }
        })
    }

    async getGasPrice() {
        const gasPrice = await this.web3.eth.getGasPrice();
        const basePriceBN = new BigNumber(gasPrice);
        const extraPriceBN = (basePriceBN.multipliedBy(new BigNumber(75))).div(new BigNumber(100));
        return basePriceBN.plus(extraPriceBN)
    }

    getTokenBalanceToken(contractAddress, contractABI, address,baseFactor) {
        return new Promise(async (resolve, reject) => {
            try {

                var contractObj = new this.web3.eth.Contract(contractABI, contractAddress);
                var balance = await contractObj.methods.balanceOf(address).call();
                var tokenAmount=this.web3.utils.toBN(balance).toString()
                 console.log(balance);  
                var valueToken= this.web3.utils.fromWei(tokenAmount, 'ether'); 
                resolve({balance:valueToken});
               
                } catch (e) {
                reject(e);
            }
        })
    }

    transferToken(contractAddress, contractABI, address, amount, baseFactor, walletAddress,privateKey) {
        return new Promise(async (resolve, reject) => {
            try {

                var contractObj = new this.web3.eth.Contract(contractABI, contractAddress);
                var balance = await contractObj.methods.balanceOf(walletAddress).call();
                var tokens = this.web3.utils.toWei(amount.toString(), 'ether');
                const decimals = this.web3.utils.toBN(18);
                const tokenAmount = this.web3.utils.toBN(tokens);
                const finalToken = this.web3.utils.toHex(tokenAmount);
                console.log(walletAddress);
               // var finalToken = this.web3.utils.toBN(amount).mul(this.web3.utils.toBN(10).pow(this.web3.utils.toBN(baseFactor))).toString();
                if (+balance < +finalToken) {
                    reject(new Error('not enough balance'));
                } else {
                    var data = contractObj.methods.transfer(address, finalToken).encodeABI();
                    let gas = await contractObj.methods.transfer(address, finalToken).estimateGas({from: walletAddress});
                    resolve(this.signTransaction(walletAddress,privateKey,data,gas,contractAddress));
                }
            } catch (e) {
                reject(e);
            }
        })
    }
    
    signTransaction(address, privateKey, functionData, gasEstimate, to) {
        return new Promise(async (resolve, reject) => {
            try {
                const nonce = await this.web3.eth.getTransactionCount(address);
                const balance = await this.web3.eth.getBalance(address);
                const balanceBN = new BigNumber(balance);
                const finalPriceBN = await this.getGasPrice();
                const estTransactionCostBN = new BigNumber(gasEstimate).multipliedBy(finalPriceBN);
                if (balanceBN.lt(estTransactionCostBN)) {
                    reject(new Error('not enough balance'));
                } else {
                    let transaction = await this.web3.eth.accounts.signTransaction({
                        to: to,
                        data: functionData,
                        gas: gasEstimate,
                        gasPrice: finalPriceBN.toString(),
                        nonce: nonce,
                        chainId: await this.web3.eth.net.getId()
                    }, privateKey);
                    let tranactionHash;
                    let that = this;
                    this.web3.eth.sendSignedTransaction(transaction.rawTransaction)
                        .on('transactionHash', function (hash) {
                            console.log("transaction hash", hash);
                            tranactionHash = hash;
                            resolve({"hash":tranactionHash});
                        })
                        .on('receipt', function (receipt) {
                            const gasUsedBN = that.web3.utils.toBN(receipt["gasUsed"]).toString();
                            const gasPrice = that.web3.utils.toBN(finalPriceBN.toString(), 10);
                            const transactionCost = that.web3.utils.toBN(gasUsedBN).mul(that.web3.utils.toBN(gasPrice));
                            receipt['transaction_cost'] = that.web3.utils.fromWei(transactionCost, 'ether');
                            receipt['gas_estimation'] = gasUsedBN.toString();
                            receipt['gas_price'] = finalPriceBN.toString();
                            resolve([receipt, nonce]);
                        })
                        .on('confirmation', function (confirmationNumber, receipt) {

                        })
                        .on('error', function (error) {
                            console.log(tranactionHash, error);
                            resolve([tranactionHash, nonce, error]);
                        });
                }
            } catch (e) {
                console.log('signTransaction', e);
                reject(e);
            }
        });
    }
 
	etherTransfer(recipientAddress,value, address, privateKey) {
        return new Promise(async (resolve, reject) => {
            try {
                var gasObj = {
                    to: recipientAddress,
                    from: address,
                    value: this.web3.utils.toHex(this.web3.utils.toWei(value, 'ether'))
                };
                var nonce = await this.web3.eth.getTransactionCount(address);
                var gasEstimate = await this.web3.eth.estimateGas(gasObj);
                var balance = await this.web3.eth.getBalance(address);
                const balanceBN = new BigNumber(balance);
                const finalPriceBN = await this.getGasPrice();
                const estTransactionCostBN = new BigNumber(gasEstimate).multipliedBy(finalPriceBN);
                if (balanceBN.lt(estTransactionCostBN)) {
                    reject({result:new Error('not enough balance'),status:false});
                } else {
                    let tranactionHash;
                    let that = this;
                    let transaction = await this.web3.eth.accounts.signTransaction({
                        to: recipientAddress,
                        value: this.web3.utils.toWei(value, 'ether'),
                        gas: gasEstimate,
                        gasPrice: finalPriceBN.toString(),
                        nonce: nonce,
                     }, privateKey);
                    this.web3.eth.sendSignedTransaction(transaction.rawTransaction)
                        .on('transactionHash', function (hash) {
                            console.log("transactionHash", hash);
                            tranactionHash = hash;
                            resolve({"result":{"tranactionHash":tranactionHash,transactionLink:"https://etherscan.io/tx/"+tranactionHash},status:true});

                        })
                        .on('receipt', function (receipt) {
                            const gasUsedBN = that.web3.utils.toBN(receipt["gasUsed"]).toString();
                            const gasPrice = that.web3.utils.toBN(finalPriceBN.toString(), 10);
                            const transactionCost = that.web3.utils.toBN(gasUsedBN).mul(that.web3.utils.toBN(gasPrice));
                            receipt['transaction_cost'] = that.web3.utils.fromWei(transactionCost, 'ether');
                            receipt['gas_estimation'] = gasUsedBN.toString();
                            receipt['gas_price'] = finalPriceBN.toString();
                            resolve([receipt]);
                        })
                        .on('confirmation', function (confirmationNumber) {

                        })
                        .on('error', function (error) {
                            // console.log(tranactionHash, error);
                            reject({"result":{"tranactionHash":tranactionHash, "nonce":nonce, "error":error.message, "finalPriceBN":finalPriceBN.toString()},status:false});
                        });
                }
            } catch (e) {
                reject(e);
            }
        })
    }
}    

module.exports =new EthereumClient();
